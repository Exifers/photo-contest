import React, {Component} from 'react';
import classNames from "classnames";
import withStyles from 'react-jss';

export const withThumbnails = (WrappedComponent) => {

    class Wrapper extends Component {
        render() {
            const {images, thumbnailClassName, imageClassName, glyphicon, ...rest} = this.props;

            return (
                <WrappedComponent {...rest}>
                    {images.map((image, index) => (
                        <div
                            key={index}
                            className={classNames(thumbnailClassName, 'm-3')}
                        >
                            <img
                                src={image}
                                className={imageClassName}
                                style={{width: '100%'}}
                            />
                            {glyphicon(index)}
                        </div>
                    ))}
                </WrappedComponent>
            )
        }
    }

    Wrapper.defaultProps = {
        images: [],
        glyphicon: () => null
    };
    return Wrapper;
};

export const withBasicStyle = (WrappedComponent) => {
    const styles = {
        image: {
            width: '100%'
        },
        thumbnail: {
            transition: 'all 0.3s',
            cursor: 'pointer',
            position: 'relative',
            '&:hover': {
                boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
            }
        }
    };

    class Wrapper extends Component {
        render() {
            const {classes, ...rest} = this.props;
            return (
                <WrappedComponent
                    thumbnailClassName={classes.thumbnail}
                    imageClassName={classes.image}
                    {...rest}
                />
            )
        }
    }

    return withStyles(styles)(Wrapper);
};

export const withThumb = (size='big') => (
    (WrappedComponent) => {
        const styles = {
            thumb: {
                color: 'rgba(0,0,0,0)',
                transition: 'all 0.4s',
                position: 'absolute',
                fontSize: '0px',
                WebkitTransform: 'rotate(-15deg)',
                MozTransform: 'rotate(-15deg)',
                MsTransform: 'rotate(-15deg)',
                OTransform: 'rotate(-15deg)',
            },
            big: {
                fontSize: '40px'
            },
            big__pos: {
                top: '20px',
                right: '20px'
            },
            small: {
                fontSize: '11px'
            },
            small__pos: {
                top: '5px',
                right: '5px',
            },
            thumb_visible: {
                color: '#13c13f'
            }
        };

        class Wrapper extends Component {
            render() {
                const {classes, votes, ...rest} = this.props;
                return (
                    <WrappedComponent
                        glyphicon={
                            (index) => (
                                <i
                                    className={classNames(
                                        "fas fa-thumbs-up",
                                        classes.thumb,
                                        classes[size + '__pos'],
                                        votes[index] && classes[size],
                                        votes[index] && classes.thumb_visible
                                    )}
                                />
                            )
                        }
                        {...rest}
                    />
                )
            }
        }

        return withStyles(styles)(Wrapper);
    }
);