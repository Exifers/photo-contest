import React, {Component} from "react";
import {SliderComponent} from '../components/SliderComponent';
import {CarouselComponent} from '../components/CarouselComponent';
import {VoteComponent} from '../components/VoteComponent';
import withStyles from 'react-jss';
import {compose} from 'redux';
import img1 from '../components/img1.jpg';
import img2 from '../components/img2.jpg';
import img3 from '../components/img3.jpg';
import img4 from '../components/img4.jpg';
import {withBasicStyle, withThumb, withThumbnails} from "../hocs/thumbnails";

const styles = {};

export class VotePane extends Component {
    constructor(props) {
        super(props);
        this.state = {
            images: [img1, img2, img3, img4, img1, img2, img3, img4],
            current: 0,
            votes: {}
        };

        this.handleImageChange = this.handleImageChange.bind(this);
        this.toggleVote = this.toggleVote.bind(this);
    }

    handleImageChange(index) {
        this.setState({current: index});
    }

    toggleVote() {
        const voted = !!this.state.votes[this.state.current];
        this.setState((state) => ({
            votes: {...state.votes, [this.state.current]: !voted}
        }));
    }

    render() {
        return (
            <div>
                <VotePane.CarouselComponent uuid={0} images={this.state.images} votes={this.state.votes}/>
                <VoteComponent voted={this.state.votes[this.state.current]} onClick={this.toggleVote}/>
                <VotePane.SliderComponent uuid={0} images={this.state.images} votes={this.state.votes}
                                      beforeChange={this.handleImageChange}/>
            </div>
        );
    }
}

VotePane.CarouselComponent = compose(withThumb('big'), withThumbnails)(CarouselComponent);
VotePane.SliderComponent = compose(withThumb('small'), withBasicStyle, withThumbnails)(SliderComponent);

VotePane = withStyles(styles)(VotePane);