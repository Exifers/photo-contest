import React, {Component} from 'react';
import ReactDom from 'react-dom';
import {VotePane} from './panes/VotePane';

ReactDom.render(
    <VotePane/>,
    document.getElementById('carousel')
);
