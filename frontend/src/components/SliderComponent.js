import React, {Component} from "react";
import withStyles from "react-jss";

const styles = {
    image: {
        width: '100%'
    },
    thumbnail: {
        transition: 'all 0.3s',
        cursor: 'pointer',
        '&:hover': {
            boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
        }
    }
};

export class SliderComponent extends Component {
    componentDidMount() {
        $(document).ready(() => {
            $('.slider-' + this.props.uuid)
                .slick({
                    centerPadding: '5px 20px',
                    centerMode: true,
                    dots: true,
                    slidesToShow: 5,
                    focusOnSelect: true,
                    infinite: false,
                    asNavFor: '.carousel-' + this.props.uuid
                })
                .on('beforeChange', (event, slick, currentSlide, nextSlide) => this.props.beforeChange(nextSlide))
                .on('afterChange', (event, slick, currentSlide) => this.props.afterChange(currentSlide));
        });
    }

    render() {
        return (
            <div className={'slider-' + this.props.uuid}>
                {this.props.children}
            </div>
        );
    }
}

SliderComponent.defaultProps = {
    uuid: 0,
    beforeChange: () => {},
    afterChange: () => {}
};

SliderComponent = withStyles(styles)(SliderComponent);