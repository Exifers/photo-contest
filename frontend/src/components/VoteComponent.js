import React, {Component} from 'react';
import withStyles from 'react-jss';
import {GlyphiconToggleButton} from '../elements/GlyphiconToggleButton';

const styles = {
    vote_panel: {
        height: '50px',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    glyphicon_button: {
        margin: 'auto 0 auto 0'
    }
};

export class VoteComponent extends Component {
    render() {
        return (
            <div className={this.props.classes.vote_panel}>
                <GlyphiconToggleButton glyphicon={'fas fa-thumbs-up'} className={this.props.classes.glyphicon_button}
                                       onClick={this.props.onClick} pressed={this.props.voted}/>
            </div>
        );
    }
}

VoteComponent.defaultProps = {
    voted: false
};

VoteComponent = withStyles(styles)(VoteComponent);