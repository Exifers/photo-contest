import React, {Component} from 'react';

export class CarouselComponent extends Component {
    componentDidMount() {
        $(document).ready(function () {
            $('.carousel-' + this.props.uuid)
                .slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    asNavFor: '.slider-' + this.props.uuid
                })
            }.bind(this)
        );
    }

    render() {
        return (
            <div className={"carousel-" + this.props.uuid}>
                {this.props.children}
            </div>
        );
    }
}
