import React, {Component} from 'react';
import classNames from 'classnames';
import withStyles from 'react-jss';

const styles = {
    glyphicon_button: {
        color: props => props.color,
        fontSize: '25px',
        cursor: 'pointer',
        transition: 'all 0.2s',
        '&:hover': {
            fontSize: '30px',
            color: props => props.hoverColor
        }
    },
    glyphicon_button_pressed: {
        color: props => props.pressedColor,
        '&:hover': {
            color: props => props.pressedHoverColor
        }
    }
};

export class GlyphiconToggleButton extends Component {
    render() {
        return (
            <i className={
                classNames(
                    this.props.glyphicon,
                    this.props.classes.glyphicon_button,
                    this.props.pressed && this.props.classes.glyphicon_button_pressed,
                    this.props.className
                )}
               onClick={() => this.props.onClick()}
            />
        );
    };
}

GlyphiconToggleButton.defaultProps = {
    pressed: false,
    glyphicon: 'fas fa-thumbs-up',
    color: '#565656',
    hoverColor: '#404040',
    pressedColor: '#14cb42',
    pressedHoverColor: '#13c13f',
    onClick: () => {
    }
};

GlyphiconToggleButton = withStyles(styles)(GlyphiconToggleButton);

