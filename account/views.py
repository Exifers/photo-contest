# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.shortcuts import render
from django.views import View

from .models import Profile, ConfirmEmailRecord
from .helpers import manage_record_and_send_email


class Register(View):
    @staticmethod
    def get(request):
        return render(request, 'account/register.html', {})

    @staticmethod
    def post(request):

        if User.objects.filter(username=request.POST.get('username')).first():
            return render(request, 'account/register.html', {
                'error_message': 'This username is already used'
            })

        if len(request.POST.get('password')) < 8:
            return render(request, 'account/register.html', {
                'error_message': 'Your password must have at least 8 characters'
            })

        if request.POST.get('password') != request.POST.get('confirmed_password'):
            return render(request, 'account/register.html', {
                'error_message': 'Passwords are different'
            })

        try:
            user = User.objects.create_user(
                username=request.POST.get('username'),
                email=request.POST.get('email'),
                password=request.POST.get('password')
            )
            profile = Profile.objects.create(user=user)
            manage_record_and_send_email(user, request.get_host())
        except ValueError:
            user.delete()
            profile.delete()
            return render(request, 'photo_contest/message', {
                'error': True
            })

        return render(request, 'photo_contest/message.html', {
            'message': 'You have been successfully registered, waiting for email confirmation'
        })


class SendEmailConfirmation(View):
    @staticmethod
    def get(request):
        manage_record_and_send_email(request.user, request.get_host())
        return render(request, 'account/email_not_confirmed.html', {
            'message': 'A new email has been sent to you'
        })


class ConfirmEmail(View):
    @staticmethod
    def get(request):
        token = request.GET.get('token', '')
        try:
            record = ConfirmEmailRecord.objects.get(token=token, used=False)
        except ConfirmEmailRecord.DoesNotExist:
            return render(request, 'photo_contest/message.html', {
                'message': 'No email confirmation request could be found'
            })
        profile = record.user.profile
        profile.email_confirmed = True
        profile.save()

        record.used = True
        record.save()

        return render(request, 'photo_contest/message.html', {
            'message': 'Your email has been confirmed.'
        })
