# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username


class ConfirmEmailRecord(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    last_update_date_time = models.DateTimeField(auto_now_add=True)
    token = models.CharField(max_length=100)
    used = models.BooleanField(default=False)

    def update(self):
        self.last_update_date_time = datetime.datetime.now()
        self.save()
