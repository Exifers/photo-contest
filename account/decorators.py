from django.shortcuts import render

from account.models import Profile


def profile_required(**kwargs):
    if 'data' not in kwargs:
        data = {}
    if 'template_name' not in kwargs:
        template_name = 'photo_contest/message.html'
        data = {
            'message': 'Your account is not a user account, create a user account to have access to this page.'
        }

    def decorator(wrapped):
        def wrapper(*args, **kwargs):
            request = args[0]
            profile = Profile.objects.filter(user=request.user).first()
            if not profile:
                return render(request, template_name, data)
            return wrapped(*args, **kwargs)
        return wrapper
    return decorator


def email_confirmed_required(**kwargs):
    if 'data' not in kwargs:
        data = {}
    if 'template_name' not in kwargs:
        template_name = 'account/email_not_confirmed.html'
        data = {}

    def decorator(wrapped):
        @profile_required()
        def wrapper(*args, **kwargs):
            request = args[0]
            if not request.user.profile.email_confirmed:
                return render(request, template_name, data)
            return wrapped(*args, **kwargs)
        return wrapper
    return decorator
