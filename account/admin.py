# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import Profile


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'


class ExtendedUserAdmin(UserAdmin):
    inlines = UserAdmin.inlines + [ProfileInline]


admin.site.unregister(User)
admin.site.register(User, ExtendedUserAdmin)
