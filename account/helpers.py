import uuid

from django.core.mail import send_mail
from django.template.loader import get_template
from django.urls import reverse

from .models import ConfirmEmailRecord


def manage_record_and_send_email(user, host):
    record, created = ConfirmEmailRecord.objects.get_or_create(
        user=user
    )
    if created:
        token = str(uuid.uuid4())
        record.token = token
        record.update()
        record.save()

    template = get_template('email/email_confirmation.html')
    url = 'http://' + host + reverse('confirm_email') + '?token=' + record.token
    context = {'url': url}

    send_mail(
        'Your account has been created',
        '',
        'phototrophy@gmail.com',
        [user.email],
        html_message=template.render(context)
    )
