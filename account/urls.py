from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^register/$', views.Register.as_view(), name='register'),
    url(r'^send_email_confirmation/$', views.SendEmailConfirmation.as_view(), name='send_email_confirmation'),
    url(r'^confirm_email/', views.ConfirmEmail.as_view(), name='confirm_email')
]