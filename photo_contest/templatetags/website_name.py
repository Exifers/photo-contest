from django import template
from mysite import website_settings

register = template.Library()


@register.simple_tag
def website_name():
    return website_settings.WEBSITE_NAME
