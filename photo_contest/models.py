# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import time

from django.contrib.auth.models import User
from django.db import models
from django.db.models import Count


class Contest(models.Model):
    theme = models.CharField(max_length=100, blank=True)

    opening_date = models.DateTimeField()
    votes_opening_date = models.DateTimeField()
    votes_closing_date = models.DateTimeField()

    def user_participated(self, user):
        if not user.is_authenticated:
            return False
        return bool(self.participation_set.filter(user=user))

    def user_voted(self, user):
        if not user.is_authenticated:
            return False
        return bool(self.participation_set.filter(vote__user=user))

    def user_votes(self, user):
        if not user.is_authenticated:
            return False
        return self.participation_set.filter(vote__user=user)

    def ranking(self):
        return self.participation_set.annotate(rank=Count('vote')).order_by('-rank')

    def __str__(self):
        return self.opening_date.strftime('%y-%m-%d') + '-' + self.theme


class Participation(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    photo = models.ImageField(upload_to='./')
    contest = models.ForeignKey(Contest, on_delete=models.CASCADE)


class Vote(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    participations = models.ManyToManyField(Participation)

    def get_contest(self):
        return self.participations.first().contest
