from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^contests/$', views.contests, name='contests'),
    url(r'^participate/$', views.ParticipateView.as_view(), name='participate'),
    url(r'^vote/$', views.VoteView.as_view(), name='vote'),
    url(r'^photo_uploaded/$', views.ParticipateView.as_view(), name='photo_uploaded')
]
