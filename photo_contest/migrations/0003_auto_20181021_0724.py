# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-10-21 07:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('photo_contest', '0002_auto_20181020_1529'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='user',
        ),
        migrations.RemoveField(
            model_name='vote',
            name='participation',
        ),
        migrations.AddField(
            model_name='vote',
            name='participations',
            field=models.ManyToManyField(to='photo_contest.Participation'),
        ),
        migrations.AlterField(
            model_name='participation',
            name='photo',
            field=models.ImageField(upload_to='./'),
        ),
        migrations.DeleteModel(
            name='Profile',
        ),
    ]
