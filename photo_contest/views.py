# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views import View

from .models import Contest, Participation, Vote
from account.decorators import email_confirmed_required


def contests(request):
    contests = Contest.objects.all()

    contests_data = map(lambda contest: {
        'contest': contest,
        'user_participated': contest.user_participated(request.user),
        'user_voted': contest.user_voted(request.user)
    }, contests)

    return render(request, 'photo_contest/contests.html', {
        'contests_data': contests_data
    })


class ParticipateView(View):
    @staticmethod
    @login_required()
    @email_confirmed_required()
    def get(request):
        contest_id = request.GET.get('contest_id')
        contest = Contest.objects.get(id=contest_id)

        participation = contest.participation_set.filter(user=request.user).first()

        return render(request, 'photo_contest/participate.html', {
            'contest': contest,
            'participation': participation
        })

    @staticmethod
    @login_required()
    @email_confirmed_required()
    def post(request):
        contest_id = request.GET.get('contest_id')
        contest = Contest.objects.get(id=contest_id)

        participation = contest.participation_set.filter(user=request.user).first()
        if 'photo' not in request.FILES:
            return render(request, 'photo_contest/participate.html', {
                'contest': contest,
                'participation': participation,
                'message': 'You did\'nt provide any photo while trying submitting a new one'
            })

        participation, created = Participation.objects.get_or_create(
            user=request.user,
            contest_id=request.GET.get('contest_id')
        )
        participation.photo = request.FILES['photo']
        participation.save()

        return render(request, 'photo_contest/photo_uploaded.html')


class VoteView(View):
    @staticmethod
    @login_required()
    @email_confirmed_required()
    def get(request):
        contest_id = request.GET.get('contest_id')
        contest = Contest.objects.get(id=contest_id)

        participations_data = map(
            lambda participation:
            {
                'participation': participation,
                'user_voted': participation in contest.user_votes(request.user)
            },
            contest.participation_set.all()
        )

        return render(request, 'photo_contest/vote.html', {
            'contest': contest,
            'participations_data': participations_data
        })

    @staticmethod
    @login_required()
    @email_confirmed_required()
    def post(request):
        contest_id = request.GET.get('contest_id')
        participations_ids = request.POST.getlist('vote')
        participations = Participation.objects.filter(id__in=participations_ids)

        vote = Vote.objects.filter(user=request.user).filter(participations__contest_id=contest_id).first()
        if not vote:
            vote = Vote.objects.create(
                user=request.user
            )
            vote.participations.set(participations)
            vote.save()
        else:
            vote.participations.set(participations)
            vote.save()
        return render(request, 'photo_contest/message.html', {
            'message': 'Your vote has been registered'
        })
