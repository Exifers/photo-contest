import os

from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import reverse_lazy
from django.views.generic import RedirectView

urlpatterns = \
    [
        url(r'^$', RedirectView.as_view(url=reverse_lazy('contests'), permanent=True)),
        url(r'^admin/', admin.site.urls),
        url(r'^photo_contest/', include('photo_contest.urls')),
        url(r'^account/', include('account.urls')),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
